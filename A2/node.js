var http = require('http');
var puerto = 3000;

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.end('Juan Carlos Acosta Perabá');
}).listen(puerto);

// Manera de mostrar por consola 1
console.log("Servidor corriendo en el puerto " + (puerto));

// Manera de mostrar por consola 2
console.log(`Servidor corriendo en el puerto ${puerto}`);
