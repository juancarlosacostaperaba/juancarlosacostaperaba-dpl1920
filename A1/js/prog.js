/* EJERCICIO PROPUESTO (ACTIVIDAD 1) */
// Strings
var nombre = "Juan Carlos";
var apellidos = "Acosta Perabá";
// Int
var edad = 20;
var anoNacimiento = 1998;

/* Hecho con alert */
// Mensaje con nombre y apellidos
alert(nombre + "\n" + apellidos);
// Mensaje con suma de edad y fecha
alert(edad+anoNacimiento);

/* Hecho con console.log */
// Mensaje con nombre y apellidos
console.log(nombre + "\n" + apellidos);
// Mensaje con suma de edad y fecha
console.log(edad+anoNacimiento);
